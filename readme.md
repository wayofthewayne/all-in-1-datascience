# Repo Breakdown 

For implementation details, (without going through the code) it is strongly recommended to go check out the documentation.pdf 

#### Twitter & neo4j 

This an automatic twitter scraper intended to be ran daily. The scraped data which was ran for about 1 month is then uploaded in a neo4j graph database. Several queries are ran and some natural language processing is also performed. 

#### Chess Visualisations 

As the name describes, a series of chess visualisation are performed from data gathered from 6 million chess games. To process this data a parallelisation technique called Map Reduce is utilized. 


#### Maltese Property Market 

Html files from 2015 to 2022 are scraped. The data is then cleaned, and features are extracted. Statistical tests and conclusions on this data are formed. Some visualisations are also presented. Finally, a predictive regression model is trained and then evaluated.


## Installation Details 

Below are the required commands to install & data files to be present for each code.
It is STRONGLY SUGGESTED to run the code from the google drive (less steps) but view the results from gitlab. The repo does not have the sufficient data files for them to be cloned and ran, but do however contain the results ran on all the data. 


THE CODE WITH ALL THE DATA READY TO BE RAN CAN BE FOUND: https://drive.google.com/file/d/15EQnRtsngDsFDD_A7g4N1fwCuXI0f_Xi/view

Twitter and Neo4j code


Installations - tweepy api - pip3 install tweepy 
	   		  - nltk - conda install -c anaconda nltk - the code base also has some lines that will install some packages within this library (present at top of file)
	   		 
(instruction is present in code base (see top of file) but examiner should ensure this is installed on the right compiler)
	 
Instructions on running - Please input bearer token instead of TOKEN_HERE in config.py (IMP)



						- Raw_Data_Wayne folder should be present (case sensitive). This is where the data is saved to
						- purged_configfile.ini should be present with the tokens all set to None. If set to finished no more data is collected since this file is responsible for keeping track of the next tokens returned by the paginator 
						- aggregate_data folder (case sensitive) should be present and contain data from ruan chaves. 
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Chess Visuliations 

Installations - pgn parsing - pip3 install chess 
			  - density plot - pip3 install mpl-scatter-density 
			  
(the above two commands are present in the code base - top of file)



data files of in pgn format should be present with these names - 
'2000_2150_elo.pgn', '2151_2250_elo.pgn', '2251_2350_elo.pgn', '2351_2450_elo.pgn', '2451_2550_elo.pgn', '2551_4000_elo.pgn'

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Analysing the maltese Property Market 


ensure 'data' file is present in the directory.Case sensetive. 
names should have format - 20150423_prop_for_sale_tom_classifieds.html
