#We begin by getting elon musk's twitter id 
import tweepy 
import config 
import pandas as pd 
import os 
import configparser 
import json 
from threading import Thread 



def purge_next_tokens(config_obj):#purge all the next tokens 
    if os.path.isfile('purged_configfile.ini') == False: 
        config_obj.add_section('next_token')
       
    config_obj.set('next_token','TWEET_TOKEN', str(config.TWEET_TOKEN))
    config_obj.set('next_token','FOLLOWING_TOKEN', str(config.FOLLOWING_TOKEN))
    config_obj.set('next_token','LIKED_TOKEN', str(config.LIKED_TWEETS_TOKEN))
    config_obj.set('next_token','FOLLOWERS_TOKEN', str(config.FOLLOWERS_TOKEN))
    config_obj.set('next_token','USER_MENTIONS_TOKEN', str(config.USER_MENTIONS_TOKEN))

    with open(r"purged_configfile.ini", 'w') as configfile:
        config_obj.write(configfile)


#load the next tokens 
def load_next_tokens(config_obj):
    config_obj.read('purged_configfile.ini')
    tokens = config_obj['next_token']

    if(tokens['TWEET_TOKEN'] == 'None'):
        config.TWEET_TOKEN = None 
    else: 
        config.TWEET_TOKEN = tokens['TWEET_TOKEN']

    if(tokens['FOLLOWING_TOKEN'] == 'None'):
        config.FOLLOWING_TOKEN = None 
    else: 
        config.FOLLOWING_TOKEN = tokens['FOLLOWING_TOKEN']

    if(tokens['LIKED_TOKEN'] == 'None'):
        config.LIKED_TWEETS_TOKEN = None 
    else: 
        config.LIKED_TWEETS_TOKEN = tokens['LIKED_TOKEN']

    if(tokens['FOLLOWERS_TOKEN'] == 'None'):
        config.FOLLOWERS_TOKEN = None 
    else: 
        config.FOLLOWERS_TOKEN = tokens['FOLLOWERS_TOKEN']
    
    if(tokens['USER_MENTIONS_TOKEN'] == 'None'):
        config.USER_MENTIONS_TOKEN = None 
    else: 
        config.USER_MENTIONS_TOKEN = tokens['USER_MENTIONS_TOKEN']


#handle the page
def handle_pagination(paginator): 

    for page in paginator: #get data from page 
        
        count = page['meta']['result_count']
        
        if('next_token' in page['meta'].keys()): #checking if next token exists 
            next_token = page['meta']['next_token']
        else: 
            next_token = "FINISHED" #case for when no more data is available 

        if(count != 0): 
            data = page['data']          
        else:     
             data = [] 
        
    return next_token,data



def hit_api_with_call(user_id, pagination_amount, amount_of_results, api_fn, tweet_fields,user_fields, next_token):
    lst_store = [] 

    for i in range(pagination_amount): #make an api call 
        paginator =  tweepy.Paginator(api_fn,user_id,max_results=amount_of_results, limit=1, pagination_token=next_token, tweet_fields=tweet_fields, user_fields=user_fields)
        next_token_ret ,data = handle_pagination(paginator)
        lst_store.extend(data)
        next_token = next_token_ret

    return lst_store, next_token #return the next token, and a list containing the data 

def save_json(data, path, file): #save data to a json file 
    filename = os.path.join(path, file)

    if os.path.isfile(filename): #if file exists, we append to json 
        mode = 'r+'
        with open(filename, mode=mode) as f:
            file_data = json.load(f)
            new_data = file_data + data #adding new data 
            f.seek(0) 
            json.dump(new_data, f, indent=2)
        f.close() 
        
    else: #else we create a json file 
        mode = 'w'
        with open(filename, mode=mode) as f:
            json.dump(data, f, indent=2)
        f.close() 
    



#MAIN ALGORITHM STARTS HERE

config_f = configparser.ConfigParser() #to purge the next tokens in a config file 

client = tweepy.Client(bearer_token=config.BEARER_TOKEN, return_type=dict) #config the client object, with the bearer token

elon_musk = client.get_users(usernames=['elonmusk'])
elon_id = elon_musk['data'][0]['id'] #get id 

#extra attributes we wish to fetch 
tweet_fields = ['author_id','created_at','public_metrics','context_annotations', 'entities']
user_fields = ['name','location']

pagination_amount  = 1 
amount_of_results = 100 

path = os.getcwd() + '/Raw_Data_Wayne' #path where to save the data 


#We load the current next tokens (so we proceed from last call) and make all the api calls, assuming there is more data to be retreived. 

load_next_tokens(config_f) #loading the purged tokens 

if(config.TWEET_TOKEN != "FINISHED"): 
    ret_list_tweets,next_token_tweets = hit_api_with_call(elon_id, pagination_amount,amount_of_results,client.get_users_tweets,tweet_fields, user_fields, config.TWEET_TOKEN)
    

if(config.FOLLOWING_TOKEN != "FINISHED"):
    ret_list_following, next_token_following = hit_api_with_call(elon_id, pagination_amount, amount_of_results, client.get_users_following,tweet_fields,user_fields, config.FOLLOWING_TOKEN)
    

if(config.LIKED_TWEETS_TOKEN != "FINISHED"):
    ret_list_likes, next_token_liked = hit_api_with_call(elon_id, pagination_amount, amount_of_results, client.get_liked_tweets,tweet_fields,user_fields, config.LIKED_TWEETS_TOKEN)
    

if(config.FOLLOWERS_TOKEN != "FINISHED"):
    ret_list_followers, next_token_followers = hit_api_with_call(elon_id, pagination_amount, amount_of_results, client.get_users_followers,tweet_fields,user_fields, config.FOLLOWERS_TOKEN)
    

if(config.USER_MENTIONS_TOKEN != "FINISHED"):
    ret_list_user_mentions, next_token_user_mentions = hit_api_with_call(elon_id, pagination_amount, amount_of_results, client.get_users_followers,tweet_fields,user_fields, config.FOLLOWERS_TOKEN)
   

 
#threading the file writing (since appending in json is 0(n))
if(config.TWEET_TOKEN != "FINISHED"):
    tweets_thread = Thread(target= save_json, args=(ret_list_tweets, path, 'tweets.json', ))
    tweets_thread.start()

	
if(config.FOLLOWING_TOKEN != "FINISHED"):
    following_thread = Thread(target= save_json, args=(ret_list_following, path, 'following.json', ))
    following_thread.start()
	

if(config.LIKED_TWEETS_TOKEN != "FINISHED"):
    likes_thread = Thread(target= save_json, args=(ret_list_likes, path, 'liked_tweets.json', ))
    likes_thread.start()
	

if(config.FOLLOWERS_TOKEN != "FINISHED"):
    followers_thread = Thread(target= save_json, args=(ret_list_followers, path, 'followers.json', ))
    followers_thread.start() 
	

if(config.USER_MENTIONS_TOKEN != "FINISHED"):
    mentions_thread = Thread(target= save_json, args=(ret_list_user_mentions, path, 'user_mentions.json', ))
    mentions_thread.start() 
	

#waiting for started threads to finish before setting the next token 
if(config.TWEET_TOKEN != "FINISHED"):
    tweets_thread.join()
    config.TWEET_TOKEN = next_token_tweets 

if(config.FOLLOWING_TOKEN != "FINISHED"):
    following_thread.join() 
    config.FOLLOWING_TOKEN = next_token_following

if(config.LIKED_TWEETS_TOKEN != "FINISHED"):
    likes_thread.join()
    config.LIKED_TWEETS_TOKEN = next_token_liked

if(config.FOLLOWERS_TOKEN != "FINISHED"):
    followers_thread.join() 
    config.FOLLOWERS_TOKEN = next_token_followers

if(config.USER_MENTIONS_TOKEN != "FINISHED"):
    mentions_thread.join() 
    config.USER_MENTIONS_TOKEN= next_token_user_mentions

purge_next_tokens(config_f) # purge the next tokens


def get_jpe_tweets(path): #function to get the tweets from handle @dr_jpe

    jpe = client.get_users(usernames=['dr_jpe'])
    jpe_id = jpe['data'][0]['id'] #get id 

    jpe_tweets, _ = hit_api_with_call(jpe_id, 1, 100,client.get_users_tweets,tweet_fields, user_fields, None)
    save_json(jpe_tweets,path,'jpe_tweets.json')

get_jpe_tweets(path)

#reset all the next tokens 

# config.TWEET_TOKEN = None
# config.FOLLOWING_TOKEN = None
# config.LIKED_TWEETS_TOKEN = None
# config.USER_MENTIONS_TOKEN =  None 
# config.FOLLOWERS_TOKEN = None 

# purge_next_tokens(config_f)
# load_next_tokens(config_f)

# print(config.TWEET_TOKEN)
# print(config.FOLLOWING_TOKEN)
# print(config.LIKED_TWEETS_TOKEN)
# print(config.USER_MENTIONS_TOKEN)
# print(config.FOLLOWERS_TOKEN)
